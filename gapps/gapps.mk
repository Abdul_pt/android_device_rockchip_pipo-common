PRODUCT_COPY_FILES += \
	$(call find-copy-subdir-files,*,device/rockchip/freakbox/gapps/system/addon.d,system/addon.d) \
	$(call find-copy-subdir-files,*,device/rockchip/freakbox/gapps/system/app,system/app) \
	$(call find-copy-subdir-files,*,device/rockchip/freakbox/gapps/system/etc,system/etc) \
	$(call find-copy-subdir-files,*,device/rockchip/freakbox/gapps/system/etc/permissions,system/etc/permissions) \
	$(call find-copy-subdir-files,*,device/rockchip/freakbox/gapps/system/etc/preferred-apps,system/etc/preferred-apps) \
	$(call find-copy-subdir-files,*,device/rockchip/freakbox/gapps/system/framework,system/framework) \
	$(call find-copy-subdir-files,*,device/rockchip/freakbox/gapps/system/lib,system/lib) \
	$(call find-copy-subdir-files,*,device/rockchip/freakbox/gapps/system/priv-app,system/priv-app) \
	$(call find-copy-subdir-files,*,device/rockchip/freakbox/gapps/system/usr/srec/en-US,system/usr/srec/en-US)
